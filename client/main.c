#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>

int main( int argc, char * argv[] )
{
  int sock;
  int nlen;
  struct sockaddr_in name;
  struct hostent *hp;
  char buf[256]="OK", rbuf[256];
  int iResult;
  struct timeval tv= {0,1000};

if (argc == 1 || argc == 3 || argc > 5)
{
    printf("Usage: %s <server_port>\nor\n", argv[0]);
    printf("Usage: %s <destination_IP> <destination_port> <message_in_quotes> [timeout_ms]\n", argv[0]);
    exit(1);
}
else
{
    if (argc == 5)
    {
        if (atoi(argv[4]) >= 1000)
        {
            tv.tv_sec = atoi(argv[4]) / 1000;
            tv.tv_usec = 1000 * (atoi(argv[4]) % 1000);
        }
        else
        {
            tv.tv_sec = 0;
            tv.tv_usec = 1000 * atoi(argv[4]);
        }
    }
}

// Create a UDP socket over IPv4
sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

if (sock < 0)
{
    printf("Failed to create socket()\n");
    exit(1);
}

if (argc == 2)
{
    printf("You are trying to run this program as if it were the server program!\n");
    printf("Switch to ../server/. and try again.\n");
}
else
{
    // CLIENT

    // Prepare to send a message: get the IP address and port number of the server specified in the command line.

    hp = gethostbyname(argv[1]);

    if (hp == 0)
        exit(1);

    // Now the name structure will be used to indicate the destination IP and port to send the message.

    // Copy the IP address to the name structure

    name.sin_family = AF_INET;
    memcpy(&name.sin_addr.s_addr, hp->h_addr, hp->h_length);
    name.sin_port = htons(atoi(argv[2]));
    // Set timeout for reception. Default is 1 ms.

    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (void*)&tv, sizeof(tv)) < 0)
    {
        perror("Error in setsockopt()\n");
    }

    // Send the message provided as a parameter

    strcpy(buf, argv[3]);

    if (sendto(sock, buf, strlen(buf) + 1, 0, (struct sockaddr_in*)&name, sizeof(name)) == -1)
    {
        printf("Failed to sendto()\n");
    }
    else
    {
        nlen = sizeof(name);

        // After sending a message, wait for a response on the same port

        if ((iResult = recvfrom(sock, rbuf, 256, 0, (struct sockaddr_in*)&name, &nlen)) == -1)
        {
            printf("Failed to recvfrom()\n");
            printf("Check if the server side is running correctly.\n");
        }
        else
        {
            printf("Received from %-lu.%-lu.%-lu.%-lu:%-d -> %s\n",
                   name.sin_addr.s_addr & 0xff,
                   (name.sin_addr.s_addr >> 8) & 0xff,
                   (name.sin_addr.s_addr >> 16) & 0xff,
                   (name.sin_addr.s_addr >> 24) & 0xff,
                   ntohs(name.sin_port), rbuf);
        }
    }
    close(sock);
 }
return 0;
}
